## Windows Commands

---

### Activate any version of Windows over the phone

```shell
slui.exe 4
```

### Reset trial period on Windows (_must be run as admin_)

```shell
slmgr.vbs /rearm
```

### Install new product key (_must be run as admin_)

`This will replace your existing key, so be sure to back it up first to be safe`

```shell
slmgr.vbs /ipk <PRODUCT_KEY_HERE>
```

### Open disk cleanup 

```shell
cleanmgr.exe
```

### Set custom disk cleanup settings 

* `numeric_id` value must be anywhere from 1 to 65535

```shell
cleanmgr.exe /SAGESET:numeric_id
```

### Run custom disk cleanup settings

* `numeric_id`value must match the number set with `/SAGESET`

```shell
cleanmgr.exe /SAGERUN:numeric_id
```

### Run disk cleanup on specific drive

* `<drive>` value should be the value of a drive currently mounted on your machine

```shell
cleanmgr.exe /d<drive>
```
For example if you wanted to clean a drive mounted with `D:` you would run the following

```shell
cleanmgr.exe /d D:
```
### Turn on Windows optional features 

```shell
optionalfeatures
```

### Open Control Panel 

```shell 
control
```

### Open Folder Options

```shell 
control folders
```

### Open Windows Update

```shell
wuapp.exe
```

`OR`

```shell
control update
```

### Open new persistent command prompt window

```shell
cmd.exe /k
```

### Check if a service is running on a given port 

`hostname/ip` value should be the value of the host your are attempting to connect to

`port_number` vale should be the port you are attempting to connect to

`If you are shown info about the service or a blinking cursor the command was successful and the port is open`

```shell 
telnet [hostname/ip] [port number]
```
### Check DNS information for a given domain

`[domain]` value should be the domain name you are looking up

```shell
nslookup
> set type all 
> [domain]
```
### View the contents of a text file

```shell
type [filename].txt
```

### Create a new empty file of a specified size (_must be run as admin_)

`[size]` value must be in bytes

```shell
fsutil file create new [filename].[ext] [size]
```

### Run constant ping against a host 

```shell 
ping [hostname/ip] -t
```

### Run constant ping with large packets

```shell 
ping [hostname/ip] -v 1024 -t
```

### Open Device Manager

```shell
devmgmt.msc
```

### Open Disk Manager

```shell
diskmgmt.msc
```

### Open System Configuration

```shell 
msconfig
```

### Open Event Viewer

```shell 
eventvwr.msc
```

### Find which version of Internet Explorer is currently installed 

```shell
reg query "HKEY_LOCAL_MACHINE\Software\Microsoft\Internet Explorer" |find "svcVersion"
```

### Get MAC address of all interfaces 

`First result is the primary interface of the machine`

```shell 
getmac
```

### Show current network configuration

```shell 
ipconfig
```

##### Show detailed network information

```shell 
ipconfig /all
```

##### Release current DHCP lease

```shell 
ipconfig /release
```

##### Request new DHCP lease 

```shell 
ipocnfig /renew
```

##### Display host DNS cache 

```shell 
ipconfig /displaydns 
```

##### Purge host DNS cache

```shell
ipconfig /flushdns
```

### Perform shutdown and reboot operations 

##### Reboot a host 

```shell 
shutdown /r
```

##### Shutdown a host 

```shell
shutdown /s
```

##### Abort a previously entered shutdown command 

```shell 
shutdown /a
```

##### Shutdown a host immediately

```shell
shutdown /f /s /t 0
```

### Create a directory

```shell
mkdir
```

`OR`

```shell
md
```

### Remove a directory 

```shell
rmdir
```

`OR`

```shell
rd
```

##### Remove a directory and its contents

`[dirname]` is the name of the directory to be removed

```shell
rmdir /S [dirname]
```

`OR`

```shell
rd /S [dirname]
```

### Change directory

```shell 
cd [dirname]
```

##### Change to the parent of the current working directory

```shell
cd ..
```

### Change to any point in the filesystem while bookmarking current position

`Absolute path must be used`

```shell 
pushd \path\to\dir
```

### Jump back to previous point in the filesystem

```shell
popd
```

### Copy file(s) to another directory

```shell
copy C:\dir1\file1.txt D:\dir2\dir3\
```

### Rename a file 

```shell
ren \path\to\file.txt \path\to\newname.txt
```

### Delete a file

```shell
del file.ext
```

### Set/Unset file attributes 

##### Set a file as Read Only

```shell
attrib +R
```

##### Unset a file as Read Only 

```shell
attrib -R
```

##### Set a file as Archive

```shell
attrib +A
```

##### Unset a file as Archive 

```shell
attrib -A
```

### Open a given file

`File must exist first`

```shell
filename.ext
```

### Display last few lines of a file 

```shell 
tail -n filename.txt
```

### Display contents of a file in real-time

```shell
tail filename.txt
```

### Show detailed system information

```shell
systeminfo
```

### Search for all occurences of a specific string in a given file 

```shell
findstr /C:"pattern to search" filename
```

### Search recurisvely through all files in current directory for a specific string

```shell
findstr /C:"pattern to search" filename
```

### Copy the output of another application to the clipboard

```shell
someApplication.exe | clip
```

